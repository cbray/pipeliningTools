# pipeliningTools
Simulation automation tools repo

This repo contains a variety of useful functions for automating simulation execution.

The following arguments are required to compile: `-std=c++11 -lX11 -lXtst -pthread`

The following arguments are recommended: `-Ofast -Wall`

To precompile the header: `g++ -std=c++11 -lX11 -lXtst -pthread -Ofast -Wall -c pipeline.h`

Go to [Gitlab pages](https://cbray.gitlab.io/pipeliningTools/pipeline_8h.html) for full documentation.

![coverage](https://gitlab.com/cbray/pipeliningTools/badges/master/build.svg)
